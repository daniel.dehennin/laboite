# Laboite

Bidule plateforme de services en ligne.

Portail de presentation des applications disponibles et des groupes utilisateurs.

Développé dans le cadre du projet [APPS-EDUCATION](https://apps.education.fr/) en collaboration avec la direction interministérielle du numérique ([DINUM](https://www.numerique.gouv.fr/dinum/)) dans le cadre du projet Rizomo.

Plus d'information sur le [ wiki](https://gitlab.mim-libre.fr/alphabet/laboite/-/wikis/home).

Utilise :
 - [METEOR](https://www.meteor.com)
 - [REACT](https://fr.reactjs.org/)

